import pygame


class Sound():

	@classmethod
	def load(cls, filename, channel_id=0):
		if filename != '':
			return cls(filename, channel_id)
		return SoundSilence()

	def __init__(self, filename, channel_id=0):
		self._sound = pygame.mixer.Sound(filename)
		self._channel = pygame.mixer.Channel(channel_id)
		self._loop: bool = False
		self._is_played = False
		self._volume = 1.0

	def is_playing(self):
		return self._channel.get_sound() is self._sound

	def is_played(self):
		return self._is_played

	def start(
		self,
		loop: bool = False,
		autoplay: bool = True,
		volume: float = 1.0,
		**kwargs,
	):
		self._loop = loop
		self._is_played = False
		self._volume = volume
		if autoplay:
			self._play()

	def restart(self):
		self._play()

	def stop(self):
		if self.is_playing():
			self._channel.stop()

	def _play(self):
		self._channel.play(
			self._sound,
			(-1 if self._loop else 0),
		)
		self._channel.set_volume(self._volume)
		self._is_played = True


class SoundSilence(Sound):

	def __init__(self, *args, **kwargs):
		pass

	def is_playing(self, *args, **kwargs):
		return False

	def is_played(self, *args, **kwargs):
		return False

	def start(self, *args, **kwargs):
		pass

	def restart(self, *args, **kwargs):
		pass

	def stop(self, *args, **kwargs):
		pass

