# sas

Simple Audio Sequencer

## Why

The idea behind `sas` is simple: I need some magical automation sugar
for my school theater event.

## What

`sas` allows you to run some sounds sequentialy.

## Dependecies

- python
- pygame>=2.1.0 (will be installed while setup)

## Setup

Clone repo:
```
git clone https://gitlab.com/fedoseevt_pets/sas.git
cd sas
```

Setup venv:
```
python3 -m venv venv
. venv/bin/activate
```

Install dependencies:
```
pip install -r requirements.txt
```

## Usage

First of all you should specify what sounds you will play. You can
make it in file `sounds.json`, in which titles of the fields are
titles of sounds, each field's value is the path to a sound file.
If the path is `""`, it whould sign a *silence* sound.

Secondly you should specify a sequence which will be played. You can do it
in the `sequence.json` file, which contains array of objects, each of them
is the "*step*" of playing, `"sound"` field specifies the title of
the sound to play. `"loop"` is an optional boolean field which indicate
if the sound will be looped. `"autoplay"` is an optional boolean field
which indicate if the sound will be played immediately at the begining
of the "*step*". `"autonext"` is an optional boolean field which indicate
if the next "*step*" will be played after the sound ended.
`"loop"` is `false` by deafult, `"autoplay"` is `true` by default,
`"autonext"` is `false` by deafult.

After that you can run application:
```
python main.py
```

It will immediately start the first "*step*".

- To restart sound press `ENTER` key.
- To start next "*step*" press `TAB` key.
- To start prevoius "*step*" press `SHIFT+TAB` keys.
- To go to the first "*step*" press `0` key.

### Report generation

You can make `sas` make you report, which contain extract of the
`sequence.json`:
```
python main.py --report
```

## Development

Install dev dependencies:
```
pip install -r dev_requirements.txt
```

Read `STYLE_NOTES.md`!

Use `flake8` to test your code style:
```
flake8
```

P.S.
I know that I can integrate `flake8` to GitLab, but I am too lazy for that!





