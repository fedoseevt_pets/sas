import sys
import configparser

import pygame

from cycle import main_loop
from tools import load_json, print_report
from app import App


if __name__ == '__main__':
	if "--report" in sys.argv:
		print_report("sequence.json")
		sys.exit(0)

	sounds = load_json('sounds.json')
	sequence = load_json('sequence.json')
	app = App(sounds, sequence)

	config = configparser.ConfigParser()
	config.read('settings.ini')

	pygame.init()
	main_loop(app, config)
	pygame.quit()





