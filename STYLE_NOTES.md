# Style Notes

The purpose of this document is to explain and define little tips, how to
write code.

## Tabs instead of spaces

If you realy want to use spaces, you can use it to support another project!
Here we use tabs only! Also recomended **tab-size is 8**, to make code less
wide.

## Line recomended size

Maximum line-length is 75. (Code should not touch the limit)

## Procedure recomended size

Recomended maximum size of a procedure is 24 lines of code (including
it's argument list and decorators). You can write long procedures, but
you should tell about that in public, so we can judge you for that.

## Classes without brackets

Classes without brackets sucks, and you too (if you write them).

Example:
```
class FuckingShit:
	...

class GodDamnNice():
	pass
```

## Ellipsis

DO NOT USE `...` INSTEAD OF `pass`!

## Blank line at end of file

As my grandfather once said: You can not mess the **kasha** with the
blank lines at end of file! Son, there is no way to stop you living
beautifully!

So now I ask you to make blank lines at end of file (at least 1, but
please do not delete my blank lines).





